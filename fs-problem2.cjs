const fs = require('fs');
// fetchData('./lipsum.txt')

function fetchData(filename) {
    //1. Read the given file lipsum.txt
    fs.readFile(filename, 'UTF-8', (error, fileData) => {
        if (error) {
            console.log(error);
        } else {
            console.log(`readed file ${filename}`);
        }
    })

    fs.writeFile('filenames.txt', '', (error) => {
        if (error) {
            console.log(error);
        }
        else {
            console.log("filename file created to store path of files");
        }
    });

    //2. Convert the content to uppercase & write to a new file. 
    //Store the name of the new file in filenames.txt
    fs.readFile(filename, 'UTF-8', (error, data) => {
        if (error) {
            console.log(error);
        }
        else {
            let fileName1 = './uppercaseFile.txt';
            fs.writeFile(fileName1, data.toUpperCase(), (error) => {
                if (error) {
                    console.log(error);
                }
                else {
                    fs.appendFile('filenames.txt', ' ' + fileName1, (error) => {
                        if (error) {
                            console.log(error);
                        }
                        else {
                            console.log("append in filenames.txt");
                        }
                    })

                    //3. Read the new file and convert it to lower case. 
                    //Then split the contents into sentences. Then write it to a new file. 
                    //Store the name of the new file in filenames.txt
                    fs.readFile(fileName1, 'UTF-8', (error, data) => {
                        if (error) {
                            console.log(error);
                        }
                        else {
                            console.log(`readed file ${fileName1}`);
                            let fileArray = data.toLowerCase().split('. ');
                            let fileData = fileArray.join('\n');
                            const fileName2 = './lowercaseFile.txt';
                            fs.writeFile(fileName2, fileData, (error) => {
                                if (error) {
                                    console.log(error);
                                }
                                else {
                                    fs.appendFile('filenames.txt', ' ' + fileName2, (error) => {
                                        if (error) {
                                            console.log(error);
                                        }
                                        else {
                                            console.log('appended to filenames.txt');
                                        }
                                    })

                                    //4. Read the new files, sort the content, write it out to a new file. 
                                    //Store the name of the new file in filenames.txt
                                    fs.readFile(fileName2, 'UTF-8', (error, data) => {
                                        if (error) {
                                            console.log(error);
                                        } else {
                                            console.log(`readed file ${fileName2}`);

                                            let sortedData = fileArray.sort();
                                            let sortedDataString = sortedData.join('')
                                            let fileName3 = './sortedFile.txt';
                                            fs.writeFile(fileName3, sortedDataString, (error) => {
                                                if (error) {
                                                    console.log(error);
                                                }
                                                else {
                                                    fs.readFile(fileName3, 'UTF-8', (error, data) => {
                                                        if (error) {
                                                            console.log(error);
                                                        } else {
                                                            console.log(`read a file ${fileName3}`);

                                                            fs.appendFile('filenames.txt', ' ' + fileName3, (error) => {
                                                                if (error) {
                                                                    console.log(error);
                                                                }
                                                                else {
                                                                    console.log("appended to filenames.txt")
                                                                }
                                                            })
                                                            // 5. Read the contents of filenames.txt and delete all 
                                                            //the new files that are mentioned in that list simultaneously.
                                                            fs.readFile('filenames.txt', "UTF-8", (error, data) => {
                                                                if (error) {
                                                                    console.log(error);
                                                                } else {
                                                                    let filesArray = data.split(' ');
                                                                    filesArray.map((file) => {
                                                                        if (file !== '') {
                                                                            setTimeout(() => {
                                                                                fs.unlink(file, (error) => {
                                                                                    if (error) {
                                                                                        console.log(error);
                                                                                    } else {
                                                                                        console.log('file is deleted successfully');
                                                                                    }
                                                                                })

                                                                            }, 3000)
                                                                        }
                                                                    })
                                                                }
                                                            })

                                                            fs.unlink('filenames.txt', (error) => {
                                                                if (error) {
                                                                    console.log(error);
                                                                } else {
                                                                    console.log('main file deleted')
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })


}

module.exports = fetchData;