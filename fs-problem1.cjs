const fs = require('fs');
const path = require('path');


function makeDirectory(dirPath, noOfFile) {
    fs.mkdir(dirPath, (error) => {
        if (error) {
            console.error("error ocuured while creating folder " + error);
            return;
        } else {
            console.log("folder created");
            creteFile(dirPath, noOfFile);
        }
    })
}

function creteFile(dirPath, noOfFile) {

    let count = 0;
    const filePathArray = [];
    for (let fileNumber = 1; fileNumber <= noOfFile; fileNumber++) {
        let filePath = path.join(dirPath, `${fileNumber}.json`);
        let data = `${fileNumber}.json file is created`
        fs.writeFile(filePath, data, (error) => {
            if (error) {
                console.error("error ocuured while creating file" + error);
                return;
            }
            else {
                count++;
                filePathArray.push(filePath);
                console.log("new file is created");
            }
            if (count == noOfFile) {
                console.log("now time to delete ")
                callBack(error, filePath);
            }
        });
    }
    function callBack(error, data){
        if(error){
            console.error(error);
        }
        else{
            deleteFiles(dirPath, filePathArray);
        }
    }
}


function deleteFiles(dirPath, filePathArray) {

    for (let fileNumber = 1; fileNumber <= filePathArray.length; fileNumber++) {
        let filePath = path.join(dirPath, `${fileNumber}.json`);
        fs.unlink(filePath, (error) => {
            if (error) {
                console.error("error ocuured to remove file" + error);
                return;
            }
            else {
                console.log("file removed");
            }
        })
    }
}

module.exports = makeDirectory;